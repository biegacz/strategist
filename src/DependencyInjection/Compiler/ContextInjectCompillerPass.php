<?php

namespace Strategist\DependencyInjection\Compiler;

use Strategist\Model\StrategyContext;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ContextInjectCompillerPass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        $context = $container->getDefinition(StrategyContext::class);
        $taggedServices = $container->findTaggedServiceIds('strategist.handler');
        $taggedServiceIds = array_keys($taggedServices);
        foreach ($taggedServiceIds as $taggedServiceId) {
            $context->addMethodCall('addProvider', [new Reference($taggedServiceId)]);
        }
    }
}