<?php

namespace Strategist;

use Strategist\DependencyInjection\Compiler\ContextInjectCompillerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class StrategistBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ContextInjectCompillerPass());
    }
}
