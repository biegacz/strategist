<?php

namespace Strategist\Model;

class StrategyContext
{
    /** @var HandlerInterface[]  */
    private $providers = [];

    /**
     * adds providers to handle incoming requests
     *
     * @param HandlerInterface $postProvider
     */
    public function addProvider(HandlerInterface $postProvider)
    {
        $this->providers[] = $postProvider;
    }

    /**
     * matches request to providers by supports method
     *
     * @param RequestInterface $request
     */
    public function handle(RequestInterface $request)
    {
        foreach ($this->providers as $provider) {
            if ($provider->supports($request)) {
                return $provider->handle($request);
            }
        }

        throw new \InvalidArgumentException('No provider handles request');
    }
}
