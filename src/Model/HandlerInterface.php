<?php

namespace Strategist\Model;

interface HandlerInterface
{
    public function handle($requestData);

    public function supports(RequestInterface $request);
}