<?php

namespace Strategist\Model;

interface RequestInterface
{
    public function getStrategy(): string;
}